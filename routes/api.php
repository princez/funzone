<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'data'], function () {
    Route::get('lotteries', 'DataApiController@lotteries');

    Route::get('lotteries/result/{lottery_id}', 'DataApiController@result');

    Route::get('news', 'DataApiController@news');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('log/in', 'AuthApiController@logIn');
    Route::post('sign/up', 'AuthApiController@signUp');
});

Route::group(['middleware' => 'jwt'], function () {
    Route::post('bid', 'BidApiController@bid');

    Route::get('/bids/{id}', 'BidApiController@getBids');

    Route::post('redeem', 'BidApiController@redeem');

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'ProfileApiController@index');

        Route::get('wallet', 'ProfileApiController@wallet');

        Route::get('wallet/recent', 'ProfileApiController@walletRecent');

        Route::post('/', 'ProfileApiController@update');
    });
});
