<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
    public $timestamps = false;
    protected $table = 'coin_wallet';
    protected $fillable = ['customer_id', 'debit', 'credit', 'transby', 'date'];

    public static function getBalance($user_id)
    {
        return Wallet::select(\DB::raw('(SUM(credit)-SUM(debit)) as balance'))
            ->whereCustomerId($user_id)
            ->first()->balance ?? 0;
    }

    public static function isValidTransaction($user_id, $amount)
    {
        return self::getBalance($user_id) >= $amount;
    }

    public static function debit($user_id, $amount, $message = '')
    {
        if (self::getBalance($user_id) < $amount) {
            return false;
        }

        return Wallet::create([
            'customer_id' => $user_id,
            'debit' => $amount,
            'transby' => $message,
            'date' => now(),
        ]);
    }
}
