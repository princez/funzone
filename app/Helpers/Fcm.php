<?php

namespace App\Helpers;
use \Session;

class Fcm 
{
	//


   public static function push($registrationIds=[],$data=[]){
    if(!count($registrationIds)){
        return [];
    }
    $key = 'AAAAszO1fdY:APA91bEmL3ZohZLONxdUSfa6AyoyB2XVs0C2cs80HF4g8BjdLu9fINLBQ7GYgEMzCysy0663fpha2CqW-j4TQbTYRMXMrP40dldOugzYCszYLOoce_34jPatawEEPYQMB69j1b6TP9_N';

    $url = 'https://fcm.googleapis.com/fcm/send';
    
    // $registrationIds = ["evzLD_nsQpGfeEbSrzeYnR:APA91bF6DCDcsut5AIDnwyRFGtTAgkPVrtOgSnbwebptoHnmSlNcN5dFn9Qa1bkQWmVF6KgtpD8weUDT2ecVV-dxGJNQXGpmI0cEsSRQJXY_Rvnv4e0BBPSSK6PImLxZH5cJJQK7Db8U"];
    
    
    $message = array( 
        // 'title'     => 'This is a title.',
        // 'body'      => 'Here is a message.',
        'vibrate'   => 1,
        'sound'      => 1
    );
     $message = array_merge($message,$data);
    
    $fields = array( 
        'registration_ids' => $registrationIds, 
        'data'             => $message
    );
    
    $headers = array( 
        'Authorization: key='.$key, 
        'Content-Type: application/json'
    );
    
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL,$url);
    curl_setopt( $ch,CURLOPT_POST,true);
    curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    // dd($result);
    return  $result;
   }
}
