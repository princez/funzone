<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Franchise extends Model
{
    //

    protected $table ="add_franchisee";
    public $timestamps = false;
}
