<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lottery extends Model
{
    //

    protected $table = 'add_company';

    public static function getLottery($id){
        return Lottery::whereStatus('A')
        ->select(
            'add_company.*',
            \DB::raw(
                '(select date from today_date where company_id = add_company.id order by date desc limit 1 ) as closingdate'
            )
        )
        ->where('add_company.id',$id)
        ->first();
    }
}
