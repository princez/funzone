<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    //
    protected $table = 'game_club';
    public $timestamps = false;
    protected $fillable = [
        'customer_id',
        'company_id',
        'beet_no',
        'beet_value',
        'date',
        'status',
    ];
}
