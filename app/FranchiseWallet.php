<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FranchiseWallet extends Model
{ 
    //
    public $timestamps = false;
    protected $table = 'wallet';
    protected $fillable = ['customer_id', 'debit', 'credit', 'transby', 'date'];
}
