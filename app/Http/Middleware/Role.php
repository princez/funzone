<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {    $USER = auth()->user();
        if(!auth()->check() || $USER->role=='user'){
            auth()->logout();
            return redirect()->route('login');
        }
       
        view()->share(['USER'=>$USER]);

        $request->merge(['USER'=>$USER,'user_id'=>$USER->id]);
                
        
        return $next($request);
    }
}
