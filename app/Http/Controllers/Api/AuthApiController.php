<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;
use \JWTAuth;
use App\User;
use App\Franchise;

class AuthApiController extends Controller
{
    //
    public function logIn(Request $req)
    {
        $input = Arr::only($req->all(), ['userid', 'password']);

        $rules = [
            'userid' => 'required',
            'password' => 'required',
        ];

        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {
            return api()->notValid([
                'errorMsg' => $validate->errors()->first(),
            ]);
        }
        $User = User::whereUserid($input['userid'])->first();
        if (!$User) {
            return api()->notValid([
                'errorMsg' => 'The userid is not registered !',
            ]);
        }

        if (md5($input['password']) != $User->password) {
            return api()->notValid([
                'errorMsg' => 'The entered password is invalid !',
            ]);
        }
        $token = JWTAuth::fromUser($User);

        return api()->success(['token' => $token]);
    }

    public function signUp(Request $req)
    {
        $input = Arr::only($req->all(), [
            'name',
            'mobile',
            'email',
            'password',
            'password_confirmation',
        ]);

        $rules = [
            'mobile' => 'required|integer|digits:10|regex:^[6789]\d{9}$^',
            'name' => 'required|min:2|max:100',
            'email' => 'required|min:2|max:200|email',
            'password' => 'required|min:4|max:15|confirmed',
            'password_confirmation' => 'required|min:4|max:15',
        ];

        $errorMessages = [
            'email.unique' =>
                'The email account is already registered . Please Login !',
        ];

        $validate = Validator::make($input, $rules, $errorMessages);
        if ($validate->fails()) {
            return api()->notValid([
                'errorMsg' => $validate->errors()->first(),
            ]);
        }
        $Franchise = Franchise::whereNotNull('id')->first();
        if (!$Franchise) {
            return api()->notValid(['errorMsg' => 'Franchise not found !']);
        }
        do {
            $userid = 'USR' . mt_rand(111111, 999999);
        } while (User::whereUserid($userid)->count());
        $input['userid'] = $userid;
        $input['password'] = md5($input['password']);
        $input = array_merge($input, [
            'state' => 'Haryana',
            'city' => 'Sirsa',
            'address' => 'Bus Stand',
            'status' => 'A',
            'date' => Date('Y-m-d'),
            'phone' => '+91' . $input['mobile'],
            'sponerid' => $Franchise->userid,
        ]);

        $User = User::create($input);
        if (!$User) {
            return api()->error();
        }

        if (!($token = JWTAuth::fromUser($User))) {
            return api()->error();
        }

        return api()->success([
            'token' => $token,
            'message' => 'Account registered successfully !',
        ]);
    }

    public function sendVerificationCode(Request $req)
    {
        $input['email'] = $req->get('email');
        $rules['email'] = 'required|email';
        $messages = [
            'email.unique' =>
                'The email account is already registered with us !',
        ];
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return api()->notValid([
                'errorMsg' => $validator->errors()->first(),
            ]);
        }

        $OTP = otp()->init($req->get('email'));

        if (!empty($OTP['error'])) {
            return api()->notValid(['errorMsg' => $OTP['error']]);
        }

        return api()->success([
            'token' => $OTP->token,
            'message' => 'An otp has been sent to ' . $input['email'],
        ]);
    }
}
