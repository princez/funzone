<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Auth;
use \Validator;
use App\Wallet;
use App\Lottery;
use App\Bid;
use App\Franchise;
use App\FranchiseWallet;
class BidApiController extends Controller
{
    //
    protected $USER, $USER_ID;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->USER = Auth::user();
            $this->USER_ID = $this->USER->id ?? null;
            return $next($request);
        });
    }
    public function bid(Request $req)
    {
        $beets = [000, 111, 222, 333, 444, 555, 666, 777, 888, 999];
        for ($i = 1; $i <= 100; $i++) {
            $beets[] = $i;
        }
        $input = Arr::only($req->all(), [
            'lottery_id',
            'beet_no',
            'beet_value',
        ]);

        $rules = [
            'lottery_id' => 'required|exists:add_company,id',
            'beet_no' => 'required|in:' . implode(',', $beets),
            'beet_value' => 'required|numeric|min:1',
        ];

        $validate = Validator::make($input, $rules);
        if ($validate->fails()) {
            return api()->notValid([
                'errorMsg' => $validate->errors()->first(),
            ]);
        }

        $Lottery = Lottery::getLottery($input['lottery_id']);

        \DB::beginTransaction();

        if (!Wallet::isValidTransaction($this->USER_ID, $input['beet_value'])) {
            return api()->notValid(['errorMsg' => 'Not Enough Points !']);
        }

        // return $Lottery->closingdate;

        $s = Bid::create(
            array_merge($input, [
                'company_id' => $input['lottery_id'],
                'customer_id' => $this->USER_ID,
                'date' => $Lottery->closingdate,
            ])
        );

        if ($s) {
            if (
                !Wallet::debit(
                    $this->USER_ID,
                    $input['beet_value'],
                    "Placed bid on $Lottery->name on " . $input['beet_no']
                )
            ) {
                return api()->error();
            }

            \DB::commit();
        }

        return api()->success([
            'message' => 'Bid Placed successfully !',
        ]);
    }

    public function redeem(Request $req)
    {
        $input = Arr::only($req->all(), ['amount']);

        $rules = [
            'amount' => 'required|numeric|min:1',
        ];

        $validate = Validator::make($input, $rules);
        if ($validate->fails()) {
            return api()->notValid([
                'errorMsg' => $validate->errors()->first(),
            ]);
        }
        $Franchise = Franchise::whereUserid($this->USER->sponerid)->first();

        \DB::beginTransaction();

        if (!Wallet::isValidTransaction($this->USER_ID, $input['amount'])) {
            return api()->notValid(['errorMsg' => 'Not Enough Points !']);
        }

        if (!Wallet::debit($this->USER_ID, $input['amount'], 'Redeem')) {
            return api()->error();
        }
        FranchiseWallet::create([
            'customer_id' => $Franchise->id,
            'credit' => $input['amount'],
            'transby' => 'Redeem From ' . $this->USER->userid,
            'date' => now(),
        ]);
        \DB::commit();

        return api()->success([
            'message' => 'Redeem request Placed successfully !',
        ]);
    }
    public function getBids($id, Request $req)
    {
        $input = Arr::only($req->all(), ['date']);
        $rules = ['date' => 'required|date'];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return api()->notValid([
                'errorMsg' => $validator->errors()->first(),
            ]);
        }
        $data = Bid::whereCompanyId($id)
            ->whereCustomerId($this->USER_ID)
            ->whereDate('date', $req->get('date'))
            ->groupBy('beet_no')
            ->select('game_club.*', \DB::raw('SUM(beet_value) as total_beet'))
            ->get();

        $totalBeets =
            Bid::whereCompanyId($id)
                ->whereCustomerId($this->USER_ID)
                ->whereDate('date', $req->get('date'))
                ->select(\DB::raw('SUM(beet_value) as total_beet'))
                ->first()->total_beet ?? 0;

        return api()->success(['data' => $data, 'total_points' => $totalBeets]);
    }
}
