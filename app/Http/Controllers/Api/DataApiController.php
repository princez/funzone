<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Auth;

use App\Lottery;
use App\Result;
use App\News;
class DataApiController extends Controller
{
    //
    protected $USER, $USER_ID;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->USER = Auth::user();
            $this->USER_ID = $this->USER->id ?? null;
            return $next($request);
        });
    }

    public function lotteries()
    {
        $data = Lottery::whereStatus('A')
            ->select(
                'add_company.*',
                \DB::raw(
                    '(select date from today_date where company_id = add_company.id order by date desc limit 1 ) as closingdate'
                )
            )
            ->get();

        return api()->success(['data' => $data]);
    }

    public function result($id)
    {
        $data = Result::whereCompanyId($id)
            ->orderBy('date', 'desc')
            ->paginate();

        return api()->success(['data' => $data]);
    }

    public function news()
    {
        $data = News::orderBy('date', 'desc')->get();

        return api()->success(['data' => $data]);
    }
}
