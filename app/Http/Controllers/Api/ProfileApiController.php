<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;
use \Auth;
use App\User;
use App\Subscription;
use App\Wallet;

class ProfileApiController extends Controller
{
    //
    protected $USER, $USER_ID;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->USER = Auth::user();
            $this->USER_ID = $this->USER->id ?? null;
            return $next($request);
        });
    }

    public function index()
    {
        return api()->success(['data' => $this->USER->getProfile()]);
    }

    public function update(Request $req)
    {
        $input = Arr::only($req->all(), [
            'name',
            'state',
            'city',
            'address',
            'password',
        ]);

        $rules = [
            'name' => 'required|min:2|max:200',
            'state' => 'required|min:2|max:100',
            'city' => 'required|min:2|max:100',
            'address' => 'required|min:2|max:300',
            'password' => 'nullable|min:4|max:20',
        ];

        $validate = Validator::make($input, $rules);
        if ($validate->fails()) {
            return api()->notValid([
                'errorMsg' => $validate->errors()->first(),
            ]);
        }

        if ($req->get('password')) {
            $input['password'] = md5($input['password']);
        }

        if ($this->USER->update($input)) {
            return api()->success([
                'message' => 'Profile updated successfully !',
            ]);
        }

        return api()->error();
    }

    public function changePassword(Request $req)
    {
        $input = Arr::only($req->all(), [
            'current_password',
            'new_password',
            'new_password_confirmation',
        ]);

        $rules = [
            'current_password' => 'required',
            'new_password' => 'required|min:6|max:20|confirmed',
            'new_password_confirmation' => 'required',
        ];

        $validate = Validator::make($input, $rules);
        if ($validate->fails()) {
            return api()->notValid([
                'errorMsg' => $validate->errors()->first(),
            ]);
        }

        $password = auth()->user()->password;
        if (!\Hash::check($input['current_password'], $password)) {
            return api()->notValid([
                'errorMsg' => 'The current password is invalid ',
            ]);
        }
        $input['password'] = bcrypt($input['new_password']);

        if ($this->USER->update($input)) {
            return api()->success([
                'message' => 'Password updated successfully !',
            ]);
        }

        return api()->error();
    }

    public function wallet()
    {
        $balance = (float) Wallet::getBalance($this->USER_ID);
        $data = Wallet::whereCustomerId($this->USER_ID)
            ->latest('id')
            ->paginate()
            ->toArray();

        $data['wallet_balance'] = $balance;

        return $data;
    }

    public function walletRecent()
    {
        $balance = (float) Wallet::getBalance($this->USER_ID);
        $data = Wallet::whereCustomerId($this->USER_ID)
            ->latest('id')
            ->whereTransby('Redeem')
            ->limit(15)
            ->get();

        return api()->success(['data' => $data, 'wallet_balance' => $balance]);
    }
}
