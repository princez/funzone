<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings	;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;



class Main implements FromCollection,ShouldAutoSize,withEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $data=[];
    protected $highlightedCellRange ="";
    public function __construct($data=[],$highlightedCellRange="A1:W1"){
        $this->data = $data;
        $this->highlightedCellRange =$highlightedCellRange; 
    }

    public function collection()
    {
        //
        return collect($this->data);
    }

    // public function headings(): array
    // {
    //     return [
    //         '#',
    //         'Name',
    //         'Email',
    //         'Created at',
    //         'Updated at'
    //     ];
    // }

    public function registerEvents(): array
    {
        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
        $cellRange = $this->highlightedCellRange;
        return [
            AfterSheet::class    => function(AfterSheet $event) use($styleArray,$cellRange){
                // $cellRange = ''; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
            },
        ];
    }

}
