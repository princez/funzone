<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    public $timestamps = false;
    protected $table = 'users_table';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userid',
        'password',
        'name',
        'state',
        'city',
        'address',
        'date',
        'email',
        'phone',
        'status',
        'sponerid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey(); // Eloquent model method
    }

    public function getJWTCustomClaims()
    {
        return [
            'user' => [
                'id' => $this->id,
            ],
        ];
    }

    public function address()
    {
        return $this->hasOne('App\Address', 'user_id');
    }

    public static function registerUser($mobile)
    {
        $User = User::create([
            'mobile' => $mobile,
        ]);

        // Address::create([
        //     'user_id'=>$User->id
        // ]);

        return $User;
    }

    public function getProfile()
    {
        $this->wallet_balance = (float) Wallet::getBalance($this->id);
        return $this;
    }
}
