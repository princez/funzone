<!DOCTYPE html>
<html>

<head>
    <title>Jax Salon</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="">



</head>

<body style="font-family: 'Poppins', sans-serif;">

    <center>
        <a href="{{ url('') }}">
            <img style="margin-top:20px;" src="{{ url('web/image/demo/logos/theme_logo.png') }}"
                alt="image" />
        </a>
    </center>
    @yield('content')
    <hr style="width:50%; border:2px solid #36ade2; background-color:#2d4052;">
    <br>
    <center>
      <a href="#" style="  text-decoration:none; font-size:20px; background-color:#36ade2; border-radius:15px; font-weight:700; color:#ffffff; padding:10px;">Jax Salon</a>
    </center>
    <br>
    </div>
    </td>
    </tr>
    </table>
</body>

</html>
